NEUTRAL = 0
MY_PAWN = 1
ENEMY_PAWN = 2

def do_turn(pw):

    # if no my planets and fleets left, return without crashing.
    if len(pw.my_planets()) == 0 and len(pw.my_fleets()) == 0:
        return

    is_there_neutral_planets = len(pw.neutral_planets()) > 0
    capturable_planets = pw.neutral_planets() + pw.enemy_planets()

    # TODO: 

    # per my planet loop
    for my_planet in pw.my_planets():

        closest_target_planet = None

        for capturable_planet in capturable_planets:
            if not (capturable_planet.owner() == ENEMY_PAWN and is_there_neutral_planets):
                if closest_target_planet is None:
                    closest_target_planet = capturable_planet
                elif pw.distance(my_planet, capturable_planet) < pw.distance(my_planet, closest_target_planet)
                    closest_target_planet = closest_target_planet

        if closest_target_planet is not None:
            if my_planet.num_ships() >= closest_target_planet.num_ships():
                pw.issue_order(my_planet, closest_target_planet)